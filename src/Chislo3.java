import java.io.*;

/**
 * Created by Dashka on 10.05.2017.
 */
public class Chislo3 {
    public static void main(String[] args) throws IOException {
        DataInputStream num = new DataInputStream(new FileInputStream("intdata.dat"));
        DataOutputStream num2 = new DataOutputStream(new FileOutputStream("int6data.dat"));
        try {
            while (true) {
                try {
                    int num1 = num.readInt();
                    if (num1 > 999 || num1 < 1000000) {
                        num2.writeInt(num1);
                    }
                } catch (EOFException a) {
                    num.close();
                    num2.close();
                }

            }
        } catch (IOException a) {
            num.close();
            num2.close();
        }
    }
}

